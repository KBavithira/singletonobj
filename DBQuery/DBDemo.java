package bcas.ap.dps;

import java.sql.SQLException;

public class DBDemo {
	
	public static void main(String[] args) {
		DBQuery dbQuery = DBQuery.getTnstance();
		System.out.println("Data inserted");
		try {
			dbQuery.insertData("Raven",3);
			dbQuery.updateData("Raven",13);
			
			dbQuery.insertData("Thanu",26);
			dbQuery.deleteData("Thanu");
			
			dbQuery.insertData("Raja",30);
			dbQuery.readData();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}

}
