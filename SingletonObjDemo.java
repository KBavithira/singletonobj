package dp.creational.singlton;

public class SingletonObjDemo {
	
	public static void main(String[] args) {
		SingletonObj obj =SingletonObj.getInstance();
		System.out.println(obj.hashCode());
		
		SingletonObj obj2= SingletonObj.getInstance();
		System.out.println(obj2.hashCode());
	}

}
