package dp.creational.singlton;

public class SingletonObj1 {
	
	private static SingletonObj singletonObj;
	
	private SingletonObj1 () {
	}
		public static SingletonObj getInstance() {
			if(singletonObj == null) {
				singletonObj = new SingletonObj();
			}
			
				
			return singletonObj;
		}
		public static void setNull() {
			singletonObj =null;
			
	}

}
